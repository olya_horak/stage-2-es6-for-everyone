import { controls } from '../../constants/controls';
import { updateHealthIndicators } from '../components/arena';

const controlsState = {
  firstBlock : false,
  secondBlock : false
}

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const eventTarget = document.getElementsByTagName('body')[0];

    eventTarget.addEventListener("keydown", event => {
      console.log(event);

      //1st fighter - critical attack => damage 2*attack, no block, 10s interval
      //2nd fighter - critical attack => damage 2*attack, no block, 10s interval

      switch (event.code) {
        case controls.PlayerOneAttack:
          var win = attack(firstFighter, secondFighter)
          if (win) {
            resolve(firstFighter);
          }
          break;
        case controls.PlayerTwoAttack:
          var win = attack(secondFighter, firstFighter)
          if (win) {
            resolve(secondFighter);
          }
          break;
        case controls.PlayerOneBlock:
          controlsState.firstBlock = true;
          break;

        case controls.PlayerTwoBlock:
          controlsState.secondBlock = true;
          break;
      }
      updateHealthIndicators(firstFighter, secondFighter);
      console.log(firstFighter, secondFighter)
    });

    eventTarget.addEventListener("keyup", event => {
      console.log(event);
      switch(event.code){
      case controls.PlayerOneBlock:
          controlsState.firstBlock = false;
          break;

        case controls.PlayerTwoBlock:
          controlsState.secondBlock = false;
          break;
      }
    });
  });
}

function attack(attacker, defender) {
  if (controlsState.firstBlock || controlsState.secondBlock)
    return;

  const damage = getDamage(attacker, defender);
  defender.currentHealth = defender.currentHealth - damage;
  if (defender.currentHealth <= 0) {
    return true;
  }
}


function handleKey(keyCode) { }
function observeKey(event) {
  console.log(event);
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);

  if (blockPower >= hitPower) {
    return 0;
  }
  const damage = hitPower - blockPower;
  return damage;
}

export function getHitPower(fighter) {
  const attack = fighter.attack;
  const base = 1;
  const criticalHitChance = (Math.random() + base);
  const hitPower = attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter) {
  const defense = fighter.defense;
  const base = 1;
  const dodgeChance = (Math.random() + base);
  const blockPower = defense * dodgeChance;
  return blockPower;
}
