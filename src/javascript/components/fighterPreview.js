import { createElement } from '../helpers/domHelper';
import { fighterService } from '../services/fightersService';

export  function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
 if (fighter === undefined) {
  fighterElement.append("waiting for player");
   return fighterElement;
 }

  const image = createFighterImage(fighter);

  fighterElement.append(image);

  const dataElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___data',
  });
  dataElement.append(fighter.name + '\n');
  dataElement.append('attack: ' + fighter.attack + '\n');
  dataElement.append('defense ' + fighter.defense + '\n');
  dataElement.append('health  ' +fighter.health + '\n');

  fighterElement.append(dataElement);
  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
