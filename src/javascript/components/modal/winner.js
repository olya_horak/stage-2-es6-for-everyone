import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function 
  const winner = fighter; 
  const winnerImage = createFighterImage(fighter);
 
  showModal({title: "Winner winner chicken dinner", bodyElement: winnerImage});
}
